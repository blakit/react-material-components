import React from 'react';
import PropTypes from 'prop-types';

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

const normalizeReduxFormProps = (WrappedComponent) => {
  const normalizeReduxFormPropsHOC = props => (
    <WrappedComponent
      {...props}
      {...props.input}
      errorText={props.meta ? props.meta.error : null}
    />
  );

  normalizeReduxFormPropsHOC.propTypes = {
    input: PropTypes.oneOfType([PropTypes.shape({})]),
    meta: PropTypes.shape({
      error: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    }),
  };

  normalizeReduxFormPropsHOC.defaultProps = {
    input: {},
    meta: {},
  };

  normalizeReduxFormPropsHOC.displayName = `normalizeReduxFormPropsHOC(${getDisplayName(WrappedComponent)})`;

  return normalizeReduxFormPropsHOC;
};

export default normalizeReduxFormProps;
