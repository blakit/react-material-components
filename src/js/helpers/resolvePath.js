export function resolvePath(object, path) {
  let s = path;
  let obj = object;
  s = s.replace(/\[(\w+)\]/g, '.$1');
  s = s.replace(/^\./, '');
  const a = s.split('.');
  for (let i = 0, n = a.length; i < n; i += 1) {
    const k = a[i];
    if (k in obj) {
      obj = obj[k];
    } else {
      return null;
    }
  }
  return obj;
}

export function someOther() {
  // to disable export default rule
}
