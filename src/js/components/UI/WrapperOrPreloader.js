import React from 'react';

import PropTypes from 'prop-types';

const WrapperOrPreloader = ({ loading, children }) => (
  loading ?
    <div className="loader">
      <div className="loader__circle" />
    </div>
    :
    children
);

WrapperOrPreloader.propTypes = {
  loading: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
WrapperOrPreloader.defaultProps = {
  children: null,
};

export default WrapperOrPreloader;
