import React from 'react';

import PropTypes from 'prop-types';

const WrapperWithPreloader = ({ loading, children }) => (
  <div>
    {loading ?
      <div className="loader loader--absolute">
        <div className="loader__circle" />
      </div>
      :
      null}
    {children}
  </div>
);

WrapperWithPreloader.propTypes = {
  loading: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
WrapperWithPreloader.defaultProps = {
  children: null,
};

export default WrapperWithPreloader;
