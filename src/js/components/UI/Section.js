import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';

const Section = (props) => {
  const {
    className,
    children,
    noContainer,
    title,
  } = props;

  const renderTitle = () => {
    if (title) {
      return (
        <div className="c-section__title">
          <h2>{title}</h2>
        </div>
      );
    }

    return null;
  };

  if (!noContainer) {
    return (
      <section className={`c-section ${className}`}>
        <Container>
          {renderTitle()}
          {React.Children.map(children, child => React.cloneElement(child))}
        </Container>
      </section>
    );
  }

  return (
    <section className={`c-section ${className}`}>
      {renderTitle()}
      {React.Children.map(children, child => React.cloneElement(child))}
    </section>
  );
};

Section.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  noContainer: PropTypes.bool,
};
Section.defaultProps = {
  className: '',
  title: '',
  children: null,
  noContainer: false,
};

export default Section;