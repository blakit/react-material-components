import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Link } from 'react-router-dom';

class Menu extends PureComponent {
  onItemClick(action) {
    const { model } = this.props;
    if (action.onClick) {
      action.onClick(model);
    }
  }

  renderLink(action) {
    const { model } = this.props;
    const { link, label } = action;
    const linkStyle = { display: 'block', textDecoration: 'none', color: 'black' };
    if (typeof link === 'function') {
      return <Link to={link(model)} style={linkStyle}>{label}</Link>;
    }
    if (typeof link === 'string') {
      return <Link to={link} style={linkStyle}>{label}</Link>;
    }
    return null;
  }

  render() {
    const { actionsGenerator, model } = this.props;
    const actions = actionsGenerator(model);
    return (
      <IconMenu
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      >
        {actions.map(action => (
          <MenuItem
            key={action.label}
            primaryText={action.link ? null : action.label}
            onClick={() => this.onItemClick(action)}
          >
            {action.link ? this.renderLink(action) : null}
          </MenuItem>
        ))}
      </IconMenu>
    );
  }
}

Menu.propTypes = {
  actionsGenerator: PropTypes.func,
  model: PropTypes.shape({}),
};

Menu.defaultProps = {
  actionsGenerator: [],
  model: {},
};

export default Menu;
