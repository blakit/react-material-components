import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';
import Row from './Row';

class TableContainer extends Component {
  renderHeader() {
    const { columns, numbered } = this.props;
    if (columns) {
      return (
        <thead>
          <tr>
            {numbered ?
              <th>#</th> : null
            }
            {columns.map(column => (
              <th key={column.header}>
                {column.header}
              </th>
            ))}
          </tr>
        </thead>
      );
    }
    return null;
  }

  renderBody() {
    const {
      columns, models, actionsGenerator, idField, numbered,
    } = this.props;
    if (columns) {
      return (
        <tbody>
          {models.map((model, i) => (
            <Row
              key={model[idField]}
              model={model}
              columns={columns}
              actionsGenerator={actionsGenerator}
              numbered={numbered}
              rowIndex={i}
            />
          ))}
        </tbody>
      );
    }
    return null;
  }

  render() {
    const { models, noModelsText } = this.props;
    if (models.length !== 0) {
      return (
        <Table responsive>
          {this.renderHeader()}
          {this.renderBody()}
        </Table>
      );
    }
    return <div className="table__not-found">{noModelsText}</div>;
  }
}

TableContainer.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})),
  models: PropTypes.arrayOf(PropTypes.shape({})),
  actionsGenerator: PropTypes.func,
  idField: PropTypes.string,
  numbered: PropTypes.bool,
  noModelsText: PropTypes.string,
};

TableContainer.defaultProps = {
  columns: [],
  models: [],
  actionsGenerator: null,
  idField: 'id',
  noModelsText: 'Нет записей',
  numbered: false,
};

export default TableContainer;
