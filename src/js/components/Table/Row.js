import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Menu from './Menu';
import { resolvePath } from '../../helpers/resolvePath';

class TableContainer extends PureComponent {
  renderMenu() {
    const { actionsGenerator, model } = this.props;
    return (
      <Menu actionsGenerator={actionsGenerator} model={model} />
    );
  }

  renderColumn(column) {
    const { model } = this.props;
    if (column.field) {
      return resolvePath(model, column.field);
    } else if (typeof column.formatter === 'function') {
      return column.formatter(model);
    } else if (column.view) {
      if (typeof column.view === 'object') {
        return React.cloneElement(column.view);
      } else if (typeof column.view === 'function') {
        return React.createElement(column.view, { model });
      }
    }
    return null;
  }

  renderRowNumber() {
    const { numbered, rowIndex } = this.props;
    if (numbered) {
      return <td>{rowIndex + 1}</td>;
    }
    return null;
  }

  render() {
    const { actionsGenerator, columns } = this.props;
    return (
      <tr>
        {this.renderRowNumber()}
        {columns.map((column, i) => (
          <td
            key={column.header}
            className={classNames({
              'no-padding': i === columns.length - 1 && actionsGenerator,
            })}
          >
            <span>{this.renderColumn(column)}</span>
            {i === columns.length - 1 && actionsGenerator ?
              this.renderMenu() : null
            }
          </td>
        ))}
      </tr>
    );
  }
}

TableContainer.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})),
  actionsGenerator: PropTypes.func,
  model: PropTypes.shape({}),
  numbered: PropTypes.bool,
  rowIndex: PropTypes.number,
};

TableContainer.defaultProps = {
  columns: null,
  actionsGenerator: null,
  model: {},
  numbered: false,
  rowIndex: null,
};

export default TableContainer;
