import React, { PureComponent } from 'react';
import { MenuItem, DropDownMenu } from 'material-ui';
import PropTypes from 'prop-types';

class Filterbar extends PureComponent {
  static renderMenu(item, idx) {
    return (
      <MenuItem
        value={item.id}
        primaryText={item.text}
        key={idx}
        checked={!!item.checked}
        onClick={item.onClick}
        onTouchTap={item.onClick}
        insetChildren
      />
    );
  }

  render() {
    const { menuItems } = this.props;
    return (
      <div className="filter">
        <DropDownMenu
          value="show"
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          open
        >
          {menuItems.map((item, idx) => Filterbar.renderMenu(item, idx))}
        </DropDownMenu>
        <div>asdddq11</div>
      </div>
    );
  }
}

Filterbar.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.shape({})),
};

Filterbar.defaultProps = {
  menuItems: [],
};

export default Filterbar;
