import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button } from 'reactstrap';

import TextInput from '../Inputs/TextInput';

import WrapperWithPreloader from '../UI/WrapperWithPreloader';

import logoDefault from '../../../images/blakit-logo.png';

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.loginFieldName = 'login';
    this.passwordFieldName = 'password';

    this.state = {
      [this.loginFieldName]: props.login,
      [this.passwordFieldName]: props.password,
    };
  }

  onSubmit(e) {
    const { onSubmit } = this.props;
    e.preventDefault();
    onSubmit({
      [this.loginFieldName]: this.state[this.loginFieldName],
      [this.passwordFieldName]: this.state[this.passwordFieldName],
    });
  }

  renderError(key) {
    const { errors } = this.props;
    if (Object.prototype.hasOwnProperty.call(errors, key)) {
      return errors[key][0];
    }
    return null;
  }

  renderLogo() {
    const { logo } = this.props;

    if (logo === false) {
      return null;
    }

    if (logo) {
      return (
        <div className="login__logo">
          <img src={logo} alt="" />
        </div>
      );
    }

    return (
      <div className="login__logo">
        <img src={logoDefault} alt="" />
      </div>
    );
  }

  render() {
    const {
      isDisabled,
      loading,
      login,
      password,
    } = this.props;

    return (
      <div className="login">
        <div className="login__inner">
          <WrapperWithPreloader loading={loading} size={90}>
            {this.renderLogo()}
            <form onSubmit={e => this.onSubmit(e)}>
              <TextInput
                name={this.loginFieldName}
                label="Login"
                value={login}
                id="login-field"
                error={this.renderError(this.loginFieldName)}
                type="text"
                onChange={val => this.setState({ [this.loginFieldName]: val })}
              />
              <TextInput
                name={this.passwordFieldName}
                label="Password"
                value={password}
                error={this.renderError(this.passwordFieldName)}
                type="password"
                id="password-field"
                onChange={val => this.setState({ [this.passwordFieldName]: val })}
              />
              <div className="form-group">
                <Button color="primary" disabled={isDisabled} block>Login</Button>
              </div>
            </form>
          </WrapperWithPreloader>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  isDisabled: PropTypes.bool,
  login: PropTypes.string,
  errors: PropTypes.objectOf(PropTypes.array),
  password: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  logo: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  loading: PropTypes.bool.isRequired,
};

LoginForm.defaultProps = {
  isDisabled: false,
  login: '',
  password: '',
  logo: null,
  errors: null,
};


export default LoginForm;
