import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import { Link } from 'react-router-dom';

class DashboardItem extends Component {
  renderImage() {
    const { image } = this.props;

    if (typeof image === 'string') {
      return <img src={image} className="dashboard-cart__image" alt="img" />;
    }
    return image;
  }

  render() {
    const {
      text,
      counter,
      title,
      image,
      counterLoading,
      loading,
      to,
      isLink,
      className,
      children,
    } = this.props;

    if (children) {
      return (
        <div className={classNames('dashboard-cart', className, { 'dashboard-cart--icon': image })}>
          {loading ?
            <div className="loader loader--absolute">
              <div className="loader__circle" />
            </div> : React.Children.map(children, child => React.cloneElement(child))}
        </div>
      );
    }

    return (
      <div className={classNames('dashboard-cart', className, { 'dashboard-cart--icon': image })}>
        {isLink ?
          <Link to={to} className="dashboard-cart__link" />
          :
          null}
        {loading ?
          <div className="loader loader--absolute">
            <div className="loader__circle" />
          </div>
          :
          <div className="dashboard-cart__inner">
            {image ?
              <div className="dashboard-cart__icon">
                {to && !isLink ?
                  <Link to={to}>
                    {this.renderImage()}
                  </Link>
                  :
                  this.renderImage()}
              </div>
              : null}

            {title ?
              <div className="dashboard-cart__title">
                {to && !isLink ?
                  <Link to={to}>
                    {title}
                  </Link>
                  :
                  title}
              </div>
              :
              null}
            {text ?
              <div className="dashboard-cart__text">{text}</div>
              :
              null}

            {counter ?
              <div className="dashboard-cart__counter">
                {counterLoading ?
                  <span className="loader-small" /> :
                  <div className="counter-text">
                    {to && !isLink ?
                      <Link to={to}>
                        {counter}
                      </Link>
                      :
                      counter}
                  </div>}
              </div>
              :
              null}
          </div>
        }
      </div>
    );
  }
}

DashboardItem.defaultProps = {
  to: null,
  className: null,
  counter: null,
  text: null,
  title: null,
  image: null,
  isLink: false,
  children: null,
  counterLoading: false,
};

DashboardItem.propTypes = {
  className: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  counterLoading: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  counter: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.object,
  ]),
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  to: PropTypes.string,
  isLink: PropTypes.bool,
};

export default DashboardItem;
