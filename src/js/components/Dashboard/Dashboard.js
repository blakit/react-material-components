import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const Dashboard = ({ className, children, size }) => (
  <div className={classNames('dashboard', className)}>
    <div className="row">
      {React.Children.map(children, child => (
        <div className={classNames({
          'col-md-4 col-sm-6 col-xs-12': !size,
          [size]: size,
        })}
        >
          {React.cloneElement(child)}
        </div>
      ))}
    </div>
  </div>
);

Dashboard.defaultProps = {
  className: null,
  size: '',
  children: null,
};

Dashboard.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
  size: PropTypes.string,
};

export default Dashboard;
