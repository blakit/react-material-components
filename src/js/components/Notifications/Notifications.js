import React, { Component } from 'react';
import { NotificationManager } from 'react-notifications';

const showNotification = (type, message, title, timeout, callback) => {
  const props = {
    type: type || 'error',
    message: message || '',
    title: title || '',
    timeout: timeout || 3000,
    callback: callback || null,
  };

  switch (type) {
    case 'info':
      return NotificationManager.info(props.message, props.title, props.timeout, props.callback);
      break;
    case 'success':
      return NotificationManager.success(props.message, props.title, props.timeout, props.callback);
      break;
    case 'warning':
      return NotificationManager.warning(props.message, props.title, props.timeout, props.callback);
      break;
    case 'error':
      return NotificationManager.error(props.message, props.title, props.timeout, props.callback);
      break;
    default:
      break;
  }
};

export default showNotification;
