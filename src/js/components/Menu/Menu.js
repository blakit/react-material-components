import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Badge,
} from 'reactstrap';

class Menu extends PureComponent {
  constructor(props) {
    super(props);

    this.handleToggle = this.handleToggle.bind(this);
    this.state = { collapsed: true };
  }

  static renderLinks(item, idx) {
    const isExact = item.to === '/';
    return (
      <NavItem key={idx}>
        <NavLink
          exact={isExact}
          to={item.to}
          active={item.active}
          className="nav-link"
        >
          {item.link} {Menu.renderBadge(item.notify, item.countLoading)}
        </NavLink>
      </NavItem>
    );
  }

  static renderBadge(notify, loading) {
    if (notify) {
      if (loading) {
        return <span className="loader-small" />;
      }

      return <Badge color="primary">{notify}</Badge>;
    }

    return null;
  }

  handleToggle() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  renderMenuItems(item, idx) {
    const isExact = item.to === '/';
    return (
      <NavItem key={idx} onClick={() => this.handleToggle()}>
        <NavLink
          exact={isExact}
          active={item.active}
          to={item.to}
          className="nav-link"
        >
          {item.link}
        </NavLink>
      </NavItem>
    );
  }

  render() {
    const { onLogoutClick, links, brand } = this.props;

    return (
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">{brand}</NavbarBrand>
        <NavbarToggler onClick={this.handleToggle} />
        <Collapse isOpen={!this.state.collapsed} navbar className="justify-content-md-between">
          <Nav className="w-100" navbar>
            {links.map((item, idx) => Menu.renderLinks(item, idx))}
          </Nav>
        </Collapse>

        <button className="btn-logout" onClick={onLogoutClick} />
      </Navbar>
    );
  }
}

Menu.propTypes = {
  onLogoutClick: PropTypes.func.isRequired,
  links: PropTypes.arrayOf(PropTypes.object),
  brand: PropTypes.string,
};

Menu.defaultProps = {
  links: [],
  brand: '',
};

export default Menu;
