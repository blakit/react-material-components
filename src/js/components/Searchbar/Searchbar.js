import React, { PureComponent } from 'react';
import { TextField } from 'material-ui';
import IconButton from 'material-ui/IconButton';
import PropTypes from 'prop-types';

class Searchbar extends PureComponent {
  render() {
    const { icon, textField, iconField } = this.props;
    return (
      <div className="search">
        <TextField
          name="name"
          {...textField}
        />
        {icon ?
          <IconButton iconClassName="muidocs-icon-custom-github" {...iconField} />
          : null}
      </div>
    );
  }
}

Searchbar.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  iconColor: PropTypes.string,
  iconHoverColor: PropTypes.string,
  icon: PropTypes.bool,
  iconField: PropTypes.shape({}),
  textField: PropTypes.shape({}),
};

Searchbar.defaultProps = {
  value: 'asd',
  iconColor: 'rgba(0, 0, 0, 0.3)',
  iconHoverColor: 'rgba(0, 0, 0, 1)',
  icon: true,
  iconField: {
    iconClassName: 'muidocs-icon-custom-github',
  },
  textField: {},
};

export default Searchbar;
