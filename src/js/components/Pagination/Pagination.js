import React, { PureComponent } from 'react';
import ReactPaginate from 'react-paginate';
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import PropTypes from 'prop-types';

class Pagination extends PureComponent {
  render() {
    return (
      <ReactPaginate
        previousLabel={<button><ChevronLeft /></button>}
        nextLabel={<ChevronRight />}
        breakLabel={<span>...</span>}
        breakClassName="break-me"
        pageRangeDisplayed={5}
        onPageChange={this.props.handlePageClick}
        containerClassName="pagination"
        subContainerClassName="pages pagination"
        activeClassName="active"
        {...this.props}
      />
    );
  }
}

Pagination.propTypes = {
  handlePageClick: PropTypes.func.isRequired,
};
Pagination.defaultProps = {};

export default Pagination;
