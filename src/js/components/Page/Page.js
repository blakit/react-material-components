import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';

class Page extends PureComponent {
  renderTitle() {
    const { title } = this.props;

    if (title) {
      return (
        <div className="p-default__title">
          <Container>
            <h1>{title}</h1>
          </Container>
        </div>
      );
    }

    return null;
  }

  renderBreadcrumbs() {
    const { breadcrumbs } = this.props;

    if (breadcrumbs) {
      return (
        <Breadcrumbs links={breadcrumbs} />
      );
    }

    return null;
  }

  render() {
    const { className, children } = this.props;

    return (
      <div>
        {this.renderBreadcrumbs()}
        <div className={`p-default ${className}`}>
          {this.renderTitle()}

          <div className="p-default__body">
            {React.Children.map(children, child => React.cloneElement(child))}
          </div>
        </div>
      </div>
    );
  }
}

Page.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  breadcrumbs: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ])),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
Page.defaultProps = {
  title: '',
  className: '',
  children: null,
  breadcrumbs: null,
};

export default Page;
