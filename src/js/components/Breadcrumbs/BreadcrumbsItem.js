import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';


const BreadcrumbsItem = ({ to, ...props }) => (
  <Link to={to} {...props} className="breadcrumbs__item" />
);

BreadcrumbsItem.propTypes = {
  to: PropTypes.string,
};
BreadcrumbsItem.defaultProps = {
  to: '',
};

export default BreadcrumbsItem;
