import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

class Breadcrumbs extends Component {
  static renderLink(link) {
    return <Link to={link.to}>{link.text}</Link>;
  }

  static renderBreadcrumbItem(link, idx) {
    return (
      <BreadcrumbItem key={idx}>
        {typeof link === 'object' ? Breadcrumbs.renderLink(link) : link}
      </BreadcrumbItem>
    );
  }

  render() {
    const { links } = this.props;

    return (
      <Breadcrumb>
        {links.map((item, idx) => Breadcrumbs.renderBreadcrumbItem(item, idx))}
      </Breadcrumb>
    );
  }
}


Breadcrumbs.propTypes = {
  links: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ])),
};
Breadcrumbs.defaultProps = {
  links: null,
};

export default Breadcrumbs;
