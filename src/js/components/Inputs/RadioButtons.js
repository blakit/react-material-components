import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RadioButtonGroup, RadioButton } from 'material-ui/RadioButton';
import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';

class RadioButtons extends Component {
  renderRadioButtons() {
    const { options } = this.props;
    if (Array.isArray(options)) {
      return options.map(option => (<RadioButton
        key={option.value}
        {...option}
      />));
    }
    return null;
  }

  render() {
    return (
      <RadioButtonGroup {...this.props}>
        {this.renderRadioButtons()}
      </RadioButtonGroup>
    );
  }
}

RadioButtons.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  })),
};

RadioButtons.defaultProps = {
  options: [],
};

export default normalizeReduxFormProps(RadioButtons);
