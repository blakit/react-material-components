import React from 'react';
import PropTypes from 'prop-types';
import MaterialCheckbox from 'material-ui/Checkbox';
import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';

const Checkbox = (props) => {
  const { onChange, value } = props;
  return (
    <MaterialCheckbox
      style={{ marginTop: 20, marginBottom: 20 }}
      checked={!!value}
      {...props}
      onCheck={onChange}
    />
  );
};

Checkbox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

Checkbox.defaultProps = {};

export default normalizeReduxFormProps(Checkbox);
