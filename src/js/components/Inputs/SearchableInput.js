import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AutoComplete from 'material-ui/AutoComplete';

import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';
import { resolvePath } from '../../helpers/resolvePath';

class SearchableInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(value) {
    this.setState({
      searchText: value,
    });
  }

  onSubmit(selected) {
    const { dataSourceConfig, onChange } = this.props;
    if (dataSourceConfig && dataSourceConfig.value) {
      onChange(resolvePath(selected, dataSourceConfig.value));
    } else {
      onChange(selected);
    }
  }

  render() {
    const {
      dataSource,
      openOnFocus,
      placeholder,
      dataSourceConfig,
    } = this.props;
    return (
      <AutoComplete
        hintText={placeholder}
        searchText={this.state.searchText}
        onUpdateInput={this.onChange}
        onNewRequest={this.onSubmit}
        dataSource={dataSource}
        dataSourceConfig={dataSourceConfig}
        filter={(searchText, key) => (key.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)}
        openOnFocus={openOnFocus}
        {...this.props}
      />
    );
  }
}

SearchableInput.propTypes = {
  dataSource: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.arrayOf(PropTypes.string),
  ]),
  openOnFocus: PropTypes.bool,
  placeholder: PropTypes.string,
  dataSourceConfig: PropTypes.shape({}),
  onChange: PropTypes.func.isRequired,
};
SearchableInput.defaultProps = {
  dataSource: [],
  openOnFocus: true,
  placeholder: '',
  dataSourceConfig: null,
};

export default normalizeReduxFormProps(SearchableInput);
