import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';

class Select extends Component {
  renderOptions() {
    const { options } = this.props;
    if (Array.isArray(options)) {
      return options.map(option => (<MenuItem
        key={option.value}
        value={option.value}
        primaryText={option.label}
      />));
    }
    return null;
  }

  render() {
    const { onChange } = this.props;
    return (
      <SelectField
        {...this.props}
        styl
        onChange={(event, index, value) => onChange(value)}
      >
        {this.renderOptions()}
      </SelectField>
    );
  }
}

Select.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType(PropTypes.string, PropTypes.shape({})),
  })),
  onChange: PropTypes.func,
};

Select.defaultProps = {
  options: [],
  onChange: () => {},
};

export default normalizeReduxFormProps(Select);
