import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import MaterialDatePicker from 'material-ui/DatePicker';
import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';

const DatePicker = props => (
  <MaterialDatePicker
    {...props}
    value={props.value ? moment(props.value).toDate() : null}
    onChange={(event, date) => props.onChange(date)}
    onBlur={() => {
    }}
  />
);

DatePicker.propTypes = {
  // locale: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.shape({}).isRequired,
};
DatePicker.defaultProps = {
  // locale: 'ru',
  onChange: () => {
  },
};

export default normalizeReduxFormProps(DatePicker);
