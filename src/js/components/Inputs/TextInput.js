import React from 'react';
import PropTypes from 'prop-types';
import { Input, Label, FormFeedback } from 'reactstrap';

import normalizeReduxFormProps from '../../HOC/normalizeReduxFormProps';

const TextInput = (props) => {
  const {
    label,
    error,
    name,
    value,
    type,
    placeholder,
    id,
    onChange,
    meta,
  } = props;

  const idArray = [];
  if (id) {
    idArray.push(id);
  }

  return (
    <div className="form-group">
      {label ? <Label for={id || ''}>{label}</Label> : null}
      <Input
        type={type || 'text'}
        name={name}
        {...idArray}
        defaultValue={value}
        placeholder={placeholder || ''}
        onChange={e => onChange(e.target.value)}
        invalid={!!error || !!meta.error}
      />
      {error || meta.error ? <FormFeedback>{error || meta.error}</FormFeedback> : null}
    </div>
  );
};

TextInput.propTypes = {
  label: PropTypes.string,
  error: PropTypes.string,
  errorText: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func,
  meta: PropTypes.shape({}),
};
TextInput.defaultProps = {
  label: '',
  error: '',
  errorText: '',
  name: '',
  value: '',
  type: '',
  placeholder: '',
  id: '',
  meta: {},
  onChange: () => {
  },
};

export default normalizeReduxFormProps(TextInput);
