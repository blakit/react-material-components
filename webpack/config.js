const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './fictive.index.js',
  output: {
    filename: 'bundle.js',
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '..', 'src/scss'),
        to: path.resolve(__dirname, '..', 'lib/scss'),
      },
      {
        from: path.resolve(__dirname, '..', 'src/images'),
        to: path.resolve(__dirname, '..', 'lib/images'),
      }
    ])
  ]
};
