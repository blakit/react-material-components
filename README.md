#React Material Components by BLAK-IT

If you adding some component you should add it to index.js in `src/js/index.js` and import in your app like that:
```sh
import { TableContainer } from 'blakit-react-material-components';
```
Run to serve
```sh
npm start
```
Run to build
```sh
npm run build
```
Run to build and publish on npm (make sure you updated version in package.json)
```sh
npm run build:publish
```